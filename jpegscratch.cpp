//
// Created by Ugo Varetto on 8/13/16.
//
#include <vector>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <string>
#include <limits>
#include <cassert>

#include <turbojpeg.h>

using namespace std;

size_t FileSize(const string& fname) {
    ifstream file(fname);
    assert(file);
    file.ignore( std::numeric_limits<std::streamsize>::max() );
    std::streamsize length = file.gcount();
    file.clear();   //  Since ignore will have set eof.
    file.seekg( 0, std::ios_base::beg );
    return length;
}


int main(int argc, char** argv) {
    //read
    if(argc < 3 ){
        cerr << "usage: " << argv[0] << " <jpeg file> <quality=[0,100]>" << endl;
    }
    const size_t length = FileSize(argv[1]);
    using Byte = unsigned char;
    vector< Byte > input(length);
    ifstream is(argv[1], ios::binary);
    is.read((char*) input.data(), input.size());
    //decompress
    long unsigned int _jpegSize = input.size(); //!< _jpegSize from above

    int jpegSubsamp, width, height;

    tjhandle _jpegDecompressor = tjInitDecompress();

    //tjDecompressHeader2(_jpegDecompressor, input.data(), _jpegSize, &width, &height, &jpegSubsamp);
    int colorSpace = 0;
    //as of libjpeg-turbo 1.5 (*2 version: no color space info)
    tjDecompressHeader3(_jpegDecompressor, input.data(),
                        _jpegSize, &width, &height, &jpegSubsamp, &colorSpace);

    const int COLOR_COMPONENTS = 3;
    //vector< Byte > buffer(width*height*COLOR_COMPONENTS); //!< will contain the decompressed image
    //Byte* buffer = tjAlloc(width*height*COLOR_COMPONENTS);
    vector< Byte > buffer(width*height*COLOR_COMPONENTS);
    tjDecompress2(_jpegDecompressor, input.data(), _jpegSize, buffer.data(), width, 0/*pitch*/, height, colorSpace, TJFLAG_FASTDCT);

    tjDestroy(_jpegDecompressor);


    //compress
    cout << "width, height: " << width << 'x' << height << endl;
    const int JPEG_QUALITY = strtol(argv[2], nullptr, 10);
    //if _jpegSize == 0 and _compressedImage == NULL a new image is allocated
    //each time tjCompress2 is invoked
    _jpegSize = buffer.size();
    unsigned char* _compressedImage = NULL; //!< Memory is allocated by tjCompress2 if _jpegSize == 0
    tjhandle _jpegCompressor = tjInitCompress();
    _compressedImage = tjAlloc(buffer.size());
    tjCompress2(_jpegCompressor, buffer.data(), width, 0, height, colorSpace,
                &_compressedImage, &_jpegSize, jpegSubsamp, JPEG_QUALITY,
                TJFLAG_FASTDCT);
    cout << "Color space: " << colorSpace << endl;
    cout << "Uncompressed size: " << buffer.size() << "\nComressed size: "
         << _jpegSize << endl;
    cout << "Compression ratio: " << double(buffer.size()) / _jpegSize << endl;
    tjDestroy(_jpegCompressor);

    //write
    ofstream os("out.jpg", ios::binary);
    assert(os);
    os.write((char*)_compressedImage, _jpegSize);

    tjFree(_compressedImage);
    //tjFree(buffer);

    return EXIT_SUCCESS;
}

/**
 * Pixel formats
 */
//enum TJPF
//{
//    /**
//     * RGB pixel format.  The red, green, and blue components in the image are
//     * stored in 3-byte pixels in the order R, G, B from lowest to highest byte
//     * address within each pixel.
//     */
//        TJPF_RGB=0,
//    /**
//     * BGR pixel format.  The red, green, and blue components in the image are
//     * stored in 3-byte pixels in the order B, G, R from lowest to highest byte
//     * address within each pixel.
//     */
//        TJPF_BGR,
//    /**
//     * RGBX pixel format.  The red, green, and blue components in the image are
//     * stored in 4-byte pixels in the order R, G, B from lowest to highest byte
//     * address within each pixel.  The X component is ignored when compressing
//     * and undefined when decompressing.
//     */
//        TJPF_RGBX,
//    /**
//     * BGRX pixel format.  The red, green, and blue components in the image are
//     * stored in 4-byte pixels in the order B, G, R from lowest to highest byte
//     * address within each pixel.  The X component is ignored when compressing
//     * and undefined when decompressing.
//     */
//        TJPF_BGRX,
//    /**
//     * XBGR pixel format.  The red, green, and blue components in the image are
//     * stored in 4-byte pixels in the order R, G, B from highest to lowest byte
//     * address within each pixel.  The X component is ignored when compressing
//     * and undefined when decompressing.
//     */
//        TJPF_XBGR,
//    /**
//     * XRGB pixel format.  The red, green, and blue components in the image are
//     * stored in 4-byte pixels in the order B, G, R from highest to lowest byte
//     * address within each pixel.  The X component is ignored when compressing
//     * and undefined when decompressing.
//     */
//        TJPF_XRGB,
//    /**
//     * Grayscale pixel format.  Each 1-byte pixel represents a luminance
//     * (brightness) level from 0 to 255.
//     */
//        TJPF_GRAY,
//    /**
//     * RGBA pixel format.  This is the same as @ref TJPF_RGBX, except that when
//     * decompressing, the X component is guaranteed to be 0xFF, which can be
//     * interpreted as an opaque alpha channel.
//     */
//        TJPF_RGBA,
//    /**
//     * BGRA pixel format.  This is the same as @ref TJPF_BGRX, except that when
//     * decompressing, the X component is guaranteed to be 0xFF, which can be
//     * interpreted as an opaque alpha channel.
//     */
//        TJPF_BGRA,
//    /**
//     * ABGR pixel format.  This is the same as @ref TJPF_XBGR, except that when
//     * decompressing, the X component is guaranteed to be 0xFF, which can be
//     * interpreted as an opaque alpha channel.
//     */
//        TJPF_ABGR,
//    /**
//     * ARGB pixel format.  This is the same as @ref TJPF_XRGB, except that when
//     * decompressing, the X component is guaranteed to be 0xFF, which can be
//     * interpreted as an opaque alpha channel.
//     */
//        TJPF_ARGB,
//    /**
//     * CMYK pixel format.  Unlike RGB, which is an additive color model used
//     * primarily for display, CMYK (Cyan/Magenta/Yellow/Key) is a subtractive
//     * color model used primarily for printing.  In the CMYK color model, the
//     * value of each color component typically corresponds to an amount of cyan,
//     * magenta, yellow, or black ink that is applied to a white background.  In
//     * order to convert between CMYK and RGB, it is necessary to use a color
//     * management system (CMS.)  A CMS will attempt to map colors within the
//     * printer's gamut to perceptually similar colors in the display's gamut and
//     * vice versa, but the mapping is typically not 1:1 or reversible, nor can it
//     * be defined with a simple formula.  Thus, such a conversion is out of scope
//     * for a codec library.  However, the TurboJPEG API allows for compressing
//     * CMYK pixels into a YCCK JPEG image (see #TJCS_YCCK) and decompressing YCCK
//     * JPEG images into CMYK pixels.
//     */
//        TJPF_CMYK
//};