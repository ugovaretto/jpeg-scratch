//
// Created by Ugo Varetto on 8/15/16.
//

#include <unordered_map>
#include <vector>
#include <list>
#include <cstring>
#include <cassert>
#include <cstdlib>
#include <turbojpeg.h>
#include <fstream>
#include <iostream>
#include <thread>
#include <algorithm>

#ifdef TIMING__
#include <chrono>
#endif

#include "SyncQueue.h"


#ifdef TIMING__
using Timer = std::chrono::steady_clock;
using Time = Timer::time_point;
using Duration = Timer::duration;
auto Tick = []{ return Timer::now(); };
auto toms = [](Duration d) {
    return std::chrono::duration_cast< std::chrono::milliseconds >(d);
};
#endif


//#define LOG__
//
//template < typename T >
//void Log(const T& v) {
//#ifdef LOG__
//    cout << v << endl;
//#endif
//}


//Note: consider using X* or *X pixel format to speed up memory access
struct HashTJPF {
    size_t operator()(TJPF n) const {
        return std::hash< int >()(int(n));
    }
};
int NumComponents(TJPF tjpgPixelFormat) {
    static std::unordered_map< TJPF, int, HashTJPF > tjpfToInt = {
        {TJPF_RGB,  3},
        {TJPF_BGR,  3},
        {TJPF_RGBX, 4},
        {TJPF_BGRX, 4},
        {TJPF_XRGB, 4},
        {TJPF_GRAY, 1},
        {TJPF_RGBA, 4},
        {TJPF_BGRA, 4},
        {TJPF_ABGR, 4},
        {TJPF_ARGB, 4},
        {TJPF_CMYK, 4}
    };
    if(tjpfToInt.find(tjpgPixelFormat) == tjpfToInt.end()) {
        throw std::domain_error("Invalid pixel format "
                                    + std::to_string(tjpgPixelFormat));
    }
    return tjpfToInt[tjpgPixelFormat];
}

void TJDeleter(unsigned char* ptr) {
    if(!ptr) return;
    tjFree(ptr);
}

class JPEGImage {
public:
    JPEGImage() : width_(0), height_(0), pixelFormat_(TJPF()),
                  subSampling_(TJSAMP()), quality_(50) {}
    JPEGImage(const JPEGImage&) = default;
    //JPEGImage(JPEGImage&&) = default;
    JPEGImage(JPEGImage&& i) {
        width_ = i.width_;
        height_ = i.height_;
        pixelFormat_ = i.pixelFormat_;
        subSampling_ = i.subSampling_;
        quality_ = i.quality_;
        data_ = std::move(i.data_);
        i.data_.reset();
        i.width_ = 0;
        i.height_ = 0;
    }
    JPEGImage(int w, int h, TJPF pf, TJSAMP s, int q) :
        width_(w), height_(h), pixelFormat_(pf), subSampling_(s), quality_(q),
        data_(tjAlloc(w * h * NumComponents(pf)), TJDeleter){} //
    JPEGImage& operator=(JPEGImage&& i) {
        width_ = i.width_;
        height_ = i.height_;
        pixelFormat_ = i.pixelFormat_;
        subSampling_ = i.subSampling_;
        quality_ = i.quality_;
        data_ = std::move(i.data_);
        i.width_ = 0;
        i.height_ = 0;
        i.data_.reset();
        return *this;
    }
    int Width() const { return width_; }
    int Height() const { return height_; }
    TJPF PixelFormat() const { return pixelFormat_; }
    TJSAMP ChrominanceSubSampling() const { return subSampling_; }
    std::shared_ptr< unsigned char > Data() const { return data_; }
    unsigned char* DataPtr() {
        return data_.get();
    }
    bool Empty() const {
        return bool(data_);
    }
    void Reset(int w, int h, TJPF pf, TJSAMP s, int quality) {
        SetParams(w, h, pf, s, quality);
        const size_t sz = std::max(tjBufSize(w, h, s),
                                   static_cast< unsigned long >(
                                       w * h * NumComponents(pf)));
        data_.reset(tjAlloc(sz),
                    TJDeleter);
    }
    void SetParams(size_t w, size_t h, TJPF pf, TJSAMP ss, int q) {
        width_ = w;
        height_ = h;
        pixelFormat_ = pf;
        subSampling_ = ss;
        quality_ = q;
    }
    void SetJPEGSize(size_t s) { jpegSize_ = s; }
    size_t JPEGSize() const { return jpegSize_; }
    bool operator!() const { return Empty(); }
private:
    int width_;
    int height_;
    TJPF pixelFormat_;
    TJSAMP subSampling_;
    int quality_;
    size_t jpegSize_;
    std::shared_ptr< unsigned char > data_;
};

size_t UncompressedSize(size_t width, size_t height, TJPF pf)  {
    return width * height * NumComponents(pf);
}

size_t UncompressedSize(const JPEGImage& i)  {
    return size_t(i.Width() * i.Height() * NumComponents(i.PixelFormat()));
}


//ADD:
// flag support
// pitch: useful to e.g. split and compress in parallel 4k images

class TJCompressor {
public:
    TJCompressor() :
        tjCompressor_(tjInitCompress()) {}
    const JPEGImage& Compress(const unsigned char* img,
                              int width,
                              int height,
                              TJPF pf,
                              TJSAMP ss,
                              int quality) {
        if(img_.Empty()
            || UncompressedSize(width, height, pf) > UncompressedSize(img_)) {
            img_.Reset(width, height, pf, ss, quality);
        }
        img_.SetParams(width, height, pf, ss, quality);
        size_t jpegSize = int(UncompressedSize(img_));
        unsigned char* ptr = img_.DataPtr();
#ifdef TIMING__
        Time begin = Tick();
#endif
        tjCompress2(tjCompressor_, img, width, 0, height, pf,
                    &ptr, &jpegSize, ss, quality,
                    TJFLAG_FASTDCT);
#ifdef TIMING__
        Time end = Tick();
        std::cout << "tjCompress2: "
             << toms(end - begin).count()
             << " ms" << std::endl;
#endif
        img_.SetJPEGSize(jpegSize);
        return img_;
    }
    ~TJCompressor() {
        tjDestroy(tjCompressor_);
    }
private:
    JPEGImage img_;
    tjhandle tjCompressor_;
};

class TJCompressor2 {
public:
    class JPEGImageWrapper {
    public:
        JPEGImageWrapper(JPEGImage img,
                         std::shared_ptr< SyncQueue< JPEGImage > > sq)
                    : img_(img), queue_(sq) {}
        const JPEGImage& Image() const { return img_; }
        operator const JPEGImage&() { return Image(); }
        void Flush() {
            queue_->Push(std::move(img_));
        }
        ~JPEGImageWrapper() {
            Flush();
        }
    private:
        JPEGImage img_;
        std::shared_ptr< SyncQueue< JPEGImage > > queue_;
    };
public:
    TJCompressor2(int numBuffers = 0,
                  size_t w = 0,
                  size_t h = 0,
                  TJPF pf = TJPF_RGB,
                  TJSAMP ss = TJSAMP_420,
                  int q = 75) :
        memoryPool_(new SyncQueue< JPEGImage >()),
        tjCompressor_(tjInitCompress()) {
        for(int i = 0; i != numBuffers; ++i) {
            memoryPool_->Push(JPEGImage(w, h, pf, ss, q));
        }
    }
    JPEGImageWrapper Compress(const unsigned char* img,
                       int width,
                       int height,
                       TJPF pf,
                       TJSAMP ss,
                       int quality) {

        JPEGImage i(memoryPool_->Pop(
                        JPEGImage(width, height, pf, ss, quality)));

        if(i.Empty()
            || UncompressedSize(width, height, pf) > UncompressedSize(i)) {
            i.Reset(width, height, pf, ss, quality);
        }
        i.SetParams(width, height, pf, ss, quality);
        size_t jpegSize = int(UncompressedSize(i));
        unsigned char* ptr = i.DataPtr();
#ifdef TIMING__
        Time begin = Tick();
#endif
        tjCompress2(tjCompressor_, img, width, 0, height, pf,
                    &ptr, &jpegSize, ss, quality,
                    TJFLAG_FASTDCT);
#ifdef TIMING__
        Time end = Tick();
        std::cout << "tjCompress2: "
             << toms(end - begin).count()
             << std::endl;
#endif
        i.SetJPEGSize(jpegSize);
        return JPEGImageWrapper(i, memoryPool_ );
    }
    void PutBack(JPEGImage&& im) {
        memoryPool_->Push(std::move(im));
    }
    void PutBack(JPEGImage& im) {
        memoryPool_->Push(std::move(im));
    }
    ~TJCompressor2() {
        tjDestroy(tjCompressor_);
    }
private:
    std::shared_ptr< SyncQueue< JPEGImage > > memoryPool_;
    tjhandle tjCompressor_;
};

enum ColorSpace {
    RGB = 0,
    RGBA,
    GRAY,
    BGR,
    BGRA,
    ABGR,
    ARGB,
    CMYK,
    RGBX,
    BGRX,
    XRGB,
};


//Note: consider using X* or *X pixel format to speed up memory access
struct HashCS {
    size_t operator()(ColorSpace n) const {
        return std::hash< int >()(int(n));
    }
};
int NComp(ColorSpace pixelFormat) {
    static std::unordered_map< ColorSpace, int, HashCS > pfToInt = {
        {RGB,  3},
        {BGR,  3},
        {GRAY, 1},
        {RGBA, 4},
        {BGRA, 4},
        {ABGR, 4},
        {ARGB, 4},
        {CMYK, 4},
        {RGBX, 4},
        {BGRX, 4},
        {XRGB, 4}
    };
    if(pfToInt.find(pixelFormat) == pfToInt.end()) {
        throw std::domain_error("Invalid pixel format "
                                    + std::to_string(pixelFormat));
    }
    return pfToInt[pixelFormat];
}

ColorSpace FromTJ(TJPF pf) {
    static std::unordered_map< TJPF, ColorSpace, HashTJPF > tjpfToCS = {
        {TJPF_RGB,  RGB},
        {TJPF_BGR,  BGR},
        {TJPF_RGBX, RGBX},
        {TJPF_BGRX, BGRX},
        {TJPF_XRGB, XRGB},
        {TJPF_GRAY, GRAY},
        {TJPF_RGBA, RGBA},
        {TJPF_BGRA, BGRA},
        {TJPF_ABGR, ABGR},
        {TJPF_ARGB, ARGB},
        {TJPF_CMYK, CMYK}
    };
    if(tjpfToCS.find(pf) == tjpfToCS.end()) {
        throw std::domain_error("Invalid pixel format "
                                    + std::to_string(pf));
    }
    return tjpfToCS[pf];
}

TJPF FromCS(ColorSpace pf) {
    static std::unordered_map< ColorSpace, TJPF, HashCS > tjpfToCS = {
        {RGB, TJPF_RGB},
        {BGR, TJPF_BGR},
        {RGBX, TJPF_RGBX},
        {BGRX, TJPF_BGRX},
        {XRGB, TJPF_XRGB},
        {GRAY, TJPF_GRAY},
        {RGBA, TJPF_RGBA},
        {BGRA, TJPF_BGRA},
        {ABGR, TJPF_ABGR},
        {ARGB, TJPF_ARGB},
        {CMYK, TJPF_CMYK}
    };
    if(tjpfToCS.find(pf) == tjpfToCS.end()) {
        throw std::domain_error("Invalid pixel format "
                                    + std::to_string(pf));
    }
    return tjpfToCS[pf];
}

class Image {
public:
    Image() : width_(0), height_(0), colorSpace_(PixelFormat()) {}
    Image(const std::vector< unsigned char >& data,
          size_t width, size_t height, ColorSpace cs) :
        data_(data), width_(width), height_(height), colorSpace_(cs) {}
    Image(Image&& i) : width_(i.width_), height_(i.height_),
                       colorSpace_(i.colorSpace_) {
        i.width_ = 0;
        i.height_ = 0;
    }
    Image(const Image&) = default;
    Image& operator=(const Image&) = default;
    size_t Width() const { return width_; }
    size_t Height() const { return height_; }
    ColorSpace PixelFormat() const { return colorSpace_; }
    std::vector< unsigned char > Data() const { return data_; }
    const unsigned char* DataPtr() const { return data_.data(); }
    unsigned char* DataPtr() { return data_.data(); }
    int NumComponents() const {
        return NComp(colorSpace_);
    }
    size_t Size() const { return width_ * height_ * NumComponents(); }
    size_t AllocatedSize() const { return data_.size(); }
    void Allocate(size_t sz) {
        data_.resize(sz);
    }
    void SetParameters(size_t w, size_t h, ColorSpace cs) {
        width_ = w;
        height_ = h;
        colorSpace_ = cs;
    }
private:
    size_t width_;
    size_t height_;
    ColorSpace colorSpace_;
    std::vector< unsigned char > data_;
};

void EmptyDeleter(const unsigned char*) {}
class TJDeCompressor {
public:
    TJDeCompressor() :
        tjDeCompressor_(tjInitDecompress()) {}
    //read data from header case
    //MAKE MOVABLE
    Image DeCompress(
        /*std::shared_ptr< const unsigned char >*/
                            const unsigned char* jpgImg,
                            size_t size) {
        int width = -1;
        int height = -1;
        int jpegSubsamp = -1;
        int colorSpace = -1;
        tjDecompressHeader3(tjDeCompressor_, jpgImg,
                            size, &width, &height, &jpegSubsamp, &colorSpace);
        const size_t uncompressedSize =
            width * height * NumComponents(TJPF(colorSpace));

        if(img_.AllocatedSize() < uncompressedSize) {
            img_.SetParameters(width, height, FromTJ(TJPF(colorSpace)));
            img_.Allocate(uncompressedSize);
        }
        img_.SetParameters(width, height, FromTJ(TJPF(colorSpace)));
#ifdef TIMING__
        Time begin = Tick();
#endif
        tjDecompress2(tjDeCompressor_, jpgImg, size, img_.DataPtr(),
                      width, 0, height, colorSpace, TJFLAG_FASTDCT);
#ifdef TIMING__
        Time end = Tick();
        std::cout << "tjDecompress2: "
             << toms(end - begin).count()
             << " ms" << std::endl;
#endif
        return img_;
    }
    ~TJDeCompressor() {
        tjDestroy(tjDeCompressor_);
    }
private:
    Image img_;
    tjhandle tjDeCompressor_;
};

using namespace std;

size_t FileSize(const string& fname) {
    ifstream file(fname);
    assert(file);
    file.ignore( std::numeric_limits<std::streamsize>::max() );
    std::streamsize length = file.gcount();
    file.clear();   //  Since ignore will have set eof.
    //file.seekg( 0, std::ios_base::beg );
    return length;
}
void TestJPGCompressor2(const unsigned char* uimg,
                        int width,
                        int height,
                        TJPF pf,
                        TJSAMP ss,
                        int quality,
                        int numImages) {
    TJCompressor2 tjc;
    for(int i = 0; i != numImages; ++i) {
//        TJCompressor2::JPEGImageWrapper iw = tjc.Compress(uimg,
//                                              width, height, pf, ss, quality);
        JPEGImage img = tjc.Compress(uimg,
                                     width, height, pf, ss, quality);
        const string fname = "out" + to_string(i) + ".jpg";

        //JPEGImage img = iw.Image();
        ofstream os(fname, ios::binary);
        assert(os);
        assert(img.DataPtr());
        os.write((char*)img.DataPtr(), img.JPEGSize());
        //tjc.PutBack(img);
        assert(img.Empty()); //moved!
    }
}


int TestCompressorAndDecompressor(int argc, char** argv) {
    //read
    if(argc < 3 ){
        cerr << "usage: " << argv[0]
             << " <jpeg file> <quality=[0,100]>" << endl;
    }
    const size_t length = FileSize(argv[1]);
    using Byte = unsigned char;
    vector< Byte > input(length);
    ifstream is(argv[1], ios::binary);
    is.read((char*) input.data(), input.size());
    Image img;
    //decompress
    TJDeCompressor decomp;
#ifdef TIMING__
    Time begin = Tick();
#endif
    img = decomp.DeCompress(input.data(), input.size());
#ifdef TIMING__
    Time end = Tick();
#endif
    //compress
    const int quality = strtol(argv[2], nullptr, 10);
    TJCompressor comp;
#ifdef TIMING__
    Time begin2 = Tick();
#endif
    JPEGImage jpegImage =
        comp.Compress(img.DataPtr(), img.Width(), img.Height(),
                      FromCS(img.PixelFormat()), TJSAMP_420, quality);
#ifdef TIMING__
    Time end2 = Tick();
#endif
    //write
    ofstream os("out.jpg", ios::binary);
    assert(os);
    os.write((const char*) jpegImage.DataPtr(), jpegImage.JPEGSize());
    os.flush();

#ifdef TIMING__
    Duration elapsed = end - begin;
    cout << "Decompression time: "
         << toms(elapsed).count()
         << "ms" << endl;

    Duration elapsed2 = end2 - begin2;
    cout << "Compression time: "
         << toms(elapsed2).count()
         << "ms" << endl;
#endif

    TestJPGCompressor2(img.DataPtr(), img.Width(), img.Height(),
                       FromCS(img.PixelFormat()), TJSAMP_420, 50, 10);

    return EXIT_SUCCESS;
}

int main(int argc, char** argv) {
   return TestCompressorAndDecompressor(argc, argv);

}